package com.example.fela;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder holder;
    private Camera camera;
    private Object cameraLock = new Object();
    private int cameraId;
    private Activity activity;
    private CameraPreviewActivityInterface activityInterface;

    public CameraPreview(Activity activity, int cameraId) {
        super(activity);
        try {
            activityInterface = (CameraPreviewActivityInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ExampleFragmentCallbackInterface ");
        }
        this.activity = activity;
        this.cameraId = cameraId;
        holder = getHolder();
        holder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Log.i("camera_api1", "Preview.surfaceCreated(..) thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
    }

    /**
     * custom camera tweaks and startPreview()
     */
    public void refreshCamera() {
        if (holder.getSurface() == null || camera == null) {
            // preview surface does not exist, camera not opened created yet
            return;
        }
        Log.i(null, "CameraPreview refreshCamera()");
        // stop preview before making changes
        try {
            camera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }
        int rotation = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
        int degrees = 0;
        // specifically for back facing camera
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 90;
                break;
            case Surface.ROTATION_90:
                degrees = 0;
                break;
            case Surface.ROTATION_180:
                degrees = 270;
                break;
            case Surface.ROTATION_270:
                degrees = 180;
                break;
        }
        camera.setDisplayOrientation(degrees);
        this.camera = camera;
        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        } catch (Exception e) {
            // this error is fixed in the camera Error Callback (Error 100)
            Log.d(VIEW_LOG_TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // if your preview can change or rotate, take care of those events here.
        // make sure to stop the preview before resizing or reformatting it.
        // do not start the camera if the tab isn't visible
//        if(activityInterface.getCurrentPage() == 1)
//            camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
//        fragment.releaseCamera();
    }

    /**
     * get the current viewPager page
     */
    public interface CameraPreviewActivityInterface {
        public int getCurrentPage();
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }
}