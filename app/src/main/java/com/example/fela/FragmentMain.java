package com.example.fela;

import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;


public class FragmentMain extends Fragment {

    // size of the image view the bitmap is going to fit into, must be global for OnGlobalLayoutListener
    private int imageViewWidth;
    private int imageViewHeight;
    private String imagePath;
    Bitmap fullBitmap;
    private ImageView imageView;
    private Button btnSend;
    private Button btnSelect;
    private File imageFile;

    // ronnys
    private static final int TAKE_PICTURE = 1;
    private String pathToFile = "";
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        // initialize views and listeners
        initComponents(view);
        Log.i("karl", "FragmentMain onCreateView");
        // listener for when imageView is created, used to resize selectedBitmap
        ViewTreeObserver vto = view.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                imageViewWidth = imageView.getWidth();
                imageViewHeight = imageView.getHeight();
                Log.d("TEST", "Height = " + imageView.getHeight() + " Width = " + imageView.getWidth());

                // handle sharing to this application (example gallery image)
                Intent intent = getActivity().getIntent();
                String action = intent.getAction();
                String type = intent.getType();
                // if something was shared to this app
                if (Intent.ACTION_SEND.equals(action) && type != null) {

                    // if a single image was shared to this app
                    if (type.startsWith("image/")) {
                        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
                        if (imageUri != null) {
                            imageFile = new File(getRealPathFromURI(imageUri));

                            // get the path of the image (required for accessing exif attributes and resizing of image)
                            imagePath = getRealPathFromURI(imageUri);
                            try {
                                ExifInterface exif = new ExifInterface(imagePath);
                                String lat = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                                String lon = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                                String date = exif.getAttribute(ExifInterface.TAG_DATETIME);

                                Log.v("karl", "lat " + lat + " lon " + lon + " date "+date);
                                // lat and lon attributes are null on the current image
                                if (lat == null || lon == null) {
                                    // do something, alert user
                                    imageFile = null;
                                    toast("Latitude and longitude attributes do not exist in image", "long");
                                } else {
                                    Log.d(null, "imagePath "+imagePath);
                                    imageView.setImageBitmap(resizeImage(imagePath));
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
                // remove the listener when finished
                ViewTreeObserver obs = view.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
            }
        });

        return view;
    }

    protected void fragmentVisible() {
        Log.i("karl", "fragmentVisible");

        File file = activity.getPictureTaken();
        if(file != null) {
            Log.i("karl","noob not null");
            ImageView img = (ImageView)getView().findViewById(R.id.iv_selected_image);


            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            fullBitmap = BitmapFactory.decodeFile(file.getAbsolutePath(),bmOptions);
            Bitmap bitmap = Bitmap.createScaledBitmap(fullBitmap,imageViewWidth,imageViewHeight,true);

            img.setImageBitmap(bitmap);
        } else {
            Log.i("karl","noob is null");
        }
    }

    protected void fragmentNotVisible() {

    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getActivity(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void initComponents(View view) {
        imageView = (ImageView) view.findViewById(R.id.iv_selected_image);
        btnSend = (Button) view.findViewById(R.id.btn_send);
        btnSelect = (Button) view.findViewById(R.id.btn_select);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageFile == null)
                    Log.i("karl","imageFile is null");

                Log.i("karl","imagePath "+imagePath);

                if(imageFile == null)
                    imageFile = activity.getPictureTaken();

                if(imageFile != null) {

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = false;
                    options.inPreferredConfig = Config.RGB_565;
                    options.inDither = true;

                    Log.i("karl", "imagePath " + imagePath);


                    Log.i(null, "imagePath " + imagePath);
                    Bitmap zeBitmap = BitmapFactory.decodeFile(imagePath, options);
                    if (zeBitmap == null) {
                        zeBitmap = fullBitmap;
                    }
                    // send current image to server
                    activity.send(((EditText) getView().findViewById(R.id.et_desc)).getText().toString(), zeBitmap, imageFile);
                    ((EditText) getView().findViewById(R.id.et_desc)).setText("".toString());
                    imageView.setImageBitmap(null);


                }
            }
        });
        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // select image from device
                select();
            }
        });
    }


    /**
     * select an image from the device (example gallery)
     */
    private void select() {
    }

    public Bitmap resizeImage(String imagePath) {
        Bitmap resizedBitmap = null;
        try {
            // orignal size of the image going ino the image view
            int inWidth = 0;
            int inHeight = 0;

            InputStream in = new FileInputStream(imagePath);

            // decode image size (decode metadata only, not the whole image)
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, options);
            in.close();
            in = null;

            // save width and height
            inWidth = options.outWidth;
            inHeight = options.outHeight;

            Log.i(null, "inWidth "+inWidth+" inHeight "+inHeight+" ivWidth "+imageViewWidth+" ivHeight "+imageViewHeight);

            // decode full image pre-resized
            in = new FileInputStream(imagePath);
            options = new BitmapFactory.Options();
            // calc rought re-size (this is no exact resize)
            options.inSampleSize = Math.max(inWidth / imageViewWidth, inHeight / imageViewHeight);
            // decode full image
            Bitmap roughBitmap = BitmapFactory.decodeStream(in, null, options);

            // calc exact destination size
            Matrix m = new Matrix();
            RectF inRect = new RectF(0, 0, roughBitmap.getWidth(), roughBitmap.getHeight());
            RectF outRect = new RectF(0, 0, imageViewWidth, imageViewHeight);
            m.setRectToRect(inRect, outRect, Matrix.ScaleToFit.CENTER);
            float[] values = new float[9];
            m.getValues(values);

            // resize bitmap
            resizedBitmap = Bitmap.createScaledBitmap(roughBitmap, (int) (roughBitmap.getWidth() * values[0]), (int) (roughBitmap.getHeight() * values[4]), true);
//            // save image
//            try {
//                FileOutputStream out = new FileOutputStream(pathOfOutputImage);
//                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
//            } catch (Exception e) {
//                Log.e("Image", e.getMessage(), e);
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resizedBitmap;
    }































    Uri uriSavedImage; // needs to be global due to bug: http://stackoverflow.com/questions/11591243/accessing-camera-using-action-image-capture?rq=1
    private static final int CAMERA_REQUEST = 100;

    private void takePicture() {
        Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        File imagesFolder = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Fela");
        if (!imagesFolder.exists()) {
            if (imagesFolder.mkdir())
                Toast.makeText(getActivity(), "DIRECTORY MADE", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getActivity(), "DIRECTORY ALREADY EXISTS", Toast.LENGTH_SHORT).show();
        }
        imageFile = new File(imagesFolder, "fela_" + Calendar.getInstance().get(Calendar.MILLISECOND) + ".jpeg");
        uriSavedImage = Uri.fromFile(imageFile);
        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        startActivityForResult(imageIntent, CAMERA_REQUEST);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data != null)
                uriSavedImage = data.getData();
            else
                Toast.makeText(getActivity(), "DATA == NULL", Toast.LENGTH_SHORT).show();
            try {
                Bitmap bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uriSavedImage);
                if (bitmapImage != null) {
                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int width = size.x;
                    int height = size.y;
                    int imageHeight = bitmapImage.getHeight();
                    int imageWidth = bitmapImage.getWidth();
                    int maxWidth = 800;
                    int round = (int) Math.round(((double) maxWidth / (double) imageWidth) * (double) imageHeight);
                    Bitmap bitmapImageScaled = Bitmap.createScaledBitmap(bitmapImage, maxWidth, round, true);
                    ((ImageView) getView().findViewById(R.id.iv_selected_image)).setImageBitmap(bitmapImageScaled);
                } else {
                    Toast.makeText(getActivity(), "bitmapImage == NULL", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * makes a toast on ui thread via runOnUiThread
     * @param message
     * @param type
     */
    public void toast(final String message, String type) {
        int duration = -1;
        if(type.equals("long"))
            duration = Toast.LENGTH_LONG;
        else if (type.equals("short"))
            duration = Toast.LENGTH_SHORT;
        final int finalDuration = Toast.LENGTH_SHORT;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(getActivity(), message, finalDuration);
                toast.show();
            }
        });
    }
}



//	private void takePicture(){
//		//Random-id-grejer-----------
//        Long timeLong = System.currentTimeMillis()/1000;
//        String tStamp = timeLong.toString();
//        //---------------------------
//
//		 File photostorage = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//		 imagesFolder = new File(photostorage, "Testname"+tStamp+".jpeg");
//         pathToFile = imagesFolder.getAbsolutePath();//ger heela sökvägen, spara denna i DB och sedan hämta denna och lägg in bilden...
//		 Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//	     i.putExtra(MediaStore.EXTRA_OUTPUT, imagesFolder.getAbsolutePath());
//	     startActivityForResult(i, TAKE_PICTURE);//startar kameran
//
//        //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        //startActivityForResult(intent, TAKE_PICTURE);
//
//	}

//    private String saveToInternalSorage(Bitmap bitmapImage){
//        ContextWrapper cw = new ContextWrapper(getActivity().getBaseContext());
//        // path to /data/data/yourapp/app_data/imageDir
//        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//        // Create imageDir
//        File mypath=new File(directory,"profile.jpg");
//
//        FileOutputStream fos = null;
//        try {
//
//            fos = new FileOutputStream(mypath);
//
//            // Use the compress method on the BitMap object to write image to the OutputStream
//            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
//            fos.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return directory.getAbsolutePath();
//    }
//
//    private void loadImageFromStorage(String path)
//    {
//
//        try {
//            File f=new File(path, "profile.jpg");
//            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
//            ImageView img=(ImageView)getView().findViewById(R.id.taken_image);
//            img.setImageBitmap(b);
//        }
//        catch (FileNotFoundException e)
//        {
//            e.printStackTrace();
//        }
//
//    }
//
//	//Ev ta bort den senare, mest för att se om det fungerar
//	private void addImageToView(Bitmap image) {
//		imageContainer.setImageBitmap(image);
//		imageContainer.setVisibility(View.VISIBLE);
//	}
//
//	private void recieveIntent(){
//		Intent incomingImage = getActivity().getIntent();
//		String intentAction = incomingImage.getAction();
//		String incomingType = incomingImage.getType();
//		//make sure it's an action and type we can handle
//		if(intentAction.equals(Intent.ACTION_SEND)){
//			if(incomingType.startsWith("image/")){
//				//EXTRA_STREAM används för saker förutom text
//				Uri receivedUri = (Uri)incomingImage.getParcelableExtra(Intent.EXTRA_STREAM);
//                Log.i("fela", "URI: " + receivedUri);
//				if (receivedUri != null) {
//				    //set the picture
//				    //RESAMPLE YOUR IMAGE DATA BEFORE DISPLAYING
//				    //picView.setImageURI(receivedUri);//just for demonstration
//					receiveImage(receivedUri);
//				}
//
//			}
//		}
//		else if(intentAction.equals(Intent.ACTION_MAIN)){
//		    //app has been launched directly, not from share list
//			//inget har "delats med.." och appen har startats normalt
//			Log.i("fela", "startats direkt");
//		}
//	}
//
//	public void receiveImage(Uri uri){
//
//		try {
//			Log.i("fela", "1");
//			Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),uri);
//			//Log.i("fela", "2");
//			//ByteArrayOutputStream bos = new ByteArrayOutputStream();//hit kommer bilden efter compress
//			//Log.i("fela", "3");
//
//            Bitmap bitmapThumb = ThumbnailUtils.extractThumbnail(bitmap, 450, 300);
//
//			imageContainer.setImageBitmap(bitmapThumb);
//			imageContainer.setVisibility(View.VISIBLE);
//			imageContainer.invalidate();
//
//            //bitmapToString(bitmap);
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//
//
//	}
