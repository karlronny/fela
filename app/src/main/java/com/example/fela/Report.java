package com.example.fela;

import android.graphics.Bitmap;

import java.util.Date;


/**
 * Created by Ronny Fredriksson on 2015-04-10.
 *
 * Class represents a report of the Fela application. This report is sent to the backend for
 * a determination who is going to have this report. This is reported as a picture of what is wrong,
 * followed by META-data and information about the picture to describe the context.
 *
 * @param picture A picture of what is reported
 * @param id ID on the DB.
 * @param date When the picture was taken
 * @param lat Latitude where picture was taken. If not specified, the value is 0.0.
 * @param lng Longitude where picture was taken. If not specified, the value is 0.0.
 * @param altitude Altitude of where picture was taken. If not specified, the value is 0.0.
 * @param locationName The name of the place(ex city) the picture was taken.
 * @param user The email-adress the user has logged in to on the device. This is used to separate user in DB of backend application.
 * @param description User gives a description of the report.
 * @param status Status of the report, e.g not started, working, problem solved, ignored etc.
 *
 */
public class Report {

    private String picture;
    private int id;
    private Date date;
    private Double lat;
    private Double lng;
    private Double altitude;
    private String locationName;
    private String user;
    private String description;
    private String status;

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi;
    }

    private String wifi;

    //Three states for describing the progress to user reporting
    protected static final String STATUS_NOT_DONE = "Not done";
    protected static final String STATUS_WORKING = "Working on it";
    protected static final String STATUS_DONE = "Work is done";

    //Standard value for description if not set
    protected static final String DESCRIPTION_NOT_SET = "Description is not specified";


    /**
     * Constructor of a report. Picture and date the picture was taken is needed.
     *
     * @param picture A picture of what is reported
     * @param date When the picture was taken
     */
    public Report(String picture, Date date) {
        this.picture = picture;
        this.date = date;
    }

    /**
     * Get the description of the report.
     *
     * @return The message a user wants to pass to where the report is sent.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of the report.
     *
     * @param description The message a user wants to pass to where the report is sent.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the email-adress the user has logged in to on the device.
     * This is used to separate user in DB of backend application.
     *
     * @return The email-adress of the device. Used as username.
     */
    public String getUser() {
        return user;
    }

    /**
     * Set the email-adress the user has logged in to on the device.
     * This is used to separate user in DB of backend application.
     *
     * @param user The email-adress of the device. Used as username.
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * Get the altitude of where picture was taken.
     *
     * @return Altitude of where picture was taken.
     */
    public Double getAltitude() {
        return altitude;
    }

    /**
     * Set the altitude of where picture was taken.
     *
     * @param altitude Altitude of where picture was taken.
     */
    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    /**
     * Get the status of the report, e.g not started, working, problem solved, ignored etc
     *
     * @return  Satus of the report.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set the status of the report, e.g not started, working, problem solved, ignored etc
     *
     * @param status Satus of the report.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Get the name of the place(ex city) the picture was taken.
     *
     * @return Name of the location the picture was taken.
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Set the name of the place(ex city) the picture was taken.
     *
     * @param locationName Name of the place the picture was taken.
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    /**
     * Get the picture representing the cause of the report.
     *
     * @return The picture object of the report.
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Set the picture representing the reason for the report.
     *
     * @param picture The picture object of the report.
     */
    public void setPicture(String picture) {
        this.picture = picture;
    }

    /**
     * Get the ID of the report created by "auto-increment" in Database.
     *
     * @return ID in Database.
     */
    public int getId() {
        return id;
    }

    /**
     * Set the ID of the report created by "auto-increment" in Database.
     *
     * @param id ID in Database.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get the Date a picture was taken.
     * @return Date a picture was taken.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Set the Date a picture was taken.
     *
     * @param date Date a picture was taken.
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Get the Latitude value.
     *
     * @return Latitude.
     */
    public Double getLat() {
        return lat;
    }

    /**
     * Set for the Latitude value.
     *
     * @param lat Latitude
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * Get Longitude value.
     *
     * @return Longitude.
     */
    public Double getLng() {
        return lng;
    }

    /**
     * Set for the Longitude value.
     *
     * @param lng Longitude
     */
    public void setLng(Double lng) {
        this.lng = lng;
    }
}