package com.example.fela;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

import android.util.Log;
/**
 * Class for making the connection to the server application. 
 * 
 * @author Ronny
 *
 */
public class TCPClient {
		
		 
		private String serverMessage;				//172.30.1.1
		public static final String SERVERIP = "192.168.43.247";//"192.168.40.09"; //Server IP address, change and re-install app
	    public static final int SERVERPORT = 4444;
	    private OnMessageReceived mMessageListener = null;
	    private boolean mRun = false;
	 
	    PrintWriter out;
	    BufferedReader in;
	 
	    public TCPClient(OnMessageReceived listener) {
	        mMessageListener = listener;
	    }
	 
	    public void sendMessage(String message){
	        if (out != null && !out.checkError()) {
	            out.println(message);
	            out.flush();
	        }
	    }
	 
	    public void stopClient(){
	        mRun = false;
	    }
	 
	    public void run() {
	 
	        mRun = true;
	 
	        try {
	            //Ip as parameter
	            InetAddress serverAddr = InetAddress.getByName(SERVERIP);
	 
	            //Socket for the connection
	            Socket socket = new Socket(serverAddr, SERVERPORT);
	            try {
	 
	                //send message server
	                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
	 
	                //receive message from server
	                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	                //in this while the client listens for the messages sent by the server
	                while (mRun) {
	                    serverMessage = in.readLine();
	                    if (serverMessage != null && mMessageListener != null) {
	                        //call the method messageReceived from MyActivity class
	                        mMessageListener.messageReceived(serverMessage);
	                    }
	                    serverMessage = null;
	 
	                }
	 
	            } catch (Exception e) {
	            } finally {
	                //closing socket after finished receiving
	                socket.close();
	            }
	 
	        } catch (Exception e) {
	        }
	 
	    }
	 
	    public interface OnMessageReceived {
	        public void messageReceived(String message);
	    }
	}

